using DataAccess.Enum;

namespace DataAccess.Model;

public class Store
{
    public Guid Id { get; set; }
    public Guid FoodCourtId { get; set; }
    public Guid CategoryId { get; set; }
    public string StoreName { get; set; }
    public string Phone { get; set; }
    public string Email { get; set; }
    public Status Status { get; set; }

    public FoodCourt FoodCourt { get; set; }
    public Category Category { get; set; }
    public ICollection<Order> Orders { get; set; }
}