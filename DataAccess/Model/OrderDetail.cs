using DataAccess.Enum;

namespace DataAccess.Model;

public class OrderDetail
{
    public Guid OrderId { get; set; }
    public Guid ProductId { get; set; }
    public decimal UnitPrice { get; set; }
    public int Quantity { get; set; }
    public decimal TaxAmount { get; set; }
    public Status Status { get; set; }
    
    public Product Product { get; set; }
    public Order Order { get; set; }
    
}