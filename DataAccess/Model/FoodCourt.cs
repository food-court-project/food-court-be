namespace DataAccess.Model;

public class FoodCourt
{
    public Guid Id { get; set; }
    public string Name { get; set; }
    public string Address { get; set; }
    public DateTime OpeningHours { get; set; }
    public DateTime ClosingHours { get; set; }

    public ICollection<Store> Stores { get; set; }
    public ICollection<Card> Cards { get; set; }
}