using DataAccess.Enum;

namespace DataAccess.Model;

public class Order
{
    public Guid Id { get; set; }
    public Store Store { get; set; }
    public Card Card { get; set; }
    public Guid StoreId { get; set; }
    public Guid CardId { get; set; }
    public DateTime OrderDate { get; set; }
    public decimal TotalAmount { get; set; }
    public double Discounts { get; set; }
    public Status Status { get; set; }

    public ICollection<OrderDetail> OrderDetails { get; set; }
}