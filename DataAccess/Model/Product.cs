using DataAccess.Enum;

namespace DataAccess.Model;

public class Product
{
    public Guid Id { get; set; }
    public Category Category { get; set; }
    public Guid CategoryId { get; set; }
    public string BarCode { get; set; }
    public string ProductName { get; set; }
    public decimal Price { get; set; }
    public Status Status { get; set; }

    public ICollection<OrderDetail> OrderDetails { get; set; }
}