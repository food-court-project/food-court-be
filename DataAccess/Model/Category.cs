using DataAccess.Enum;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata.Internal;

namespace DataAccess.Model;

public class Category
{
    public Guid Id { get; set; }
    public CategoryGenre Genre { get; set; }
    public Status Status { get; set; }

    public ICollection<Product> Products { get; set; }
}