using DataAccess.Enum;

namespace DataAccess.Model;

public class Transaction
{
    public Guid Id { get; set; }
    public Card Card { get; set; }
    public Guid CardId { get; set; }
    public decimal Amount { get; set; }
    public DateTime TransactionTime { get; set; }
    public TransactionType TransactionType { get; set; }
    
}