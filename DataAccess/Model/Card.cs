using DataAccess.Enum;

namespace DataAccess.Model;

public class Card
{
    public Guid Id { get; set; }
    public FoodCourt FoodCourt { get; set; }
    public Guid FoodCourtId { get; set; }
    public string CardHolder { get; set; }
    public decimal Balance { get; set; }
    public DateTime ExpirationDate { get; set; }
    public Status Status { get; set; }

    public ICollection<Transaction> Transactions { get; set; }
    public ICollection<Order> Orders { get; set; }
}