namespace DataAccess.Enum;

public enum TransactionType
{
    Deposit,
    Pay
}