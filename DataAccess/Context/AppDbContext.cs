using DataAccess.Model;
using Microsoft.EntityFrameworkCore;

namespace DataAccess.Context;

public class AppDbContext : DbContext
{
    public DbSet<FoodCourt> FoodCourts { get; set; }
    public DbSet<Card> Cards { get; set; }
    public DbSet<Category> Categories { get; set; }
    public DbSet<Order> Orders { get; set; }
    public DbSet<OrderDetail> OrderDetails { get; set; }
    public DbSet<Product> Products { get; set; }
    public DbSet<Store> Stores { get; set; }
    public DbSet<Transaction> Transactions { get; set; }

    public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
    {
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        base.OnModelCreating(modelBuilder);

        modelBuilder.Entity<Product>()
            .Property(x => x.Price)
            .HasColumnType("decimal(18, 2)");

        modelBuilder.Entity<Card>()
            .Property(x => x.Balance)
            .HasColumnType("decimal(18, 2)");

        modelBuilder.Entity<Order>()
            .Property(x => x.TotalAmount)
            .HasColumnType("decimal(18, 2)");

        modelBuilder.Entity<OrderDetail>()
            .Property(x => x.TaxAmount)
            .HasColumnType("decimal(18, 2)");

        modelBuilder.Entity<OrderDetail>()
            .Property(x => x.UnitPrice)
            .HasColumnType("decimal(18, 2)");

        modelBuilder.Entity<Transaction>()
            .Property(x => x.Amount)
            .HasColumnType("decimal(18, 2)");
    }
}