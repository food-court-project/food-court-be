using DataAccess.Context;
using DataAccess.Repository.IRepository;
using Microsoft.EntityFrameworkCore;

namespace DataAccess.Repository;

public class GenericRepository<T> : IGenericRepository<T> where T : class
{
    private readonly AppDbContext _context;
    private readonly DbSet<T> _dbSet;

    public GenericRepository(AppDbContext context)
    {
        _context = context;
        _dbSet = context.Set<T>();
    }
    public List<T> GetAll()
    {
        return _dbSet.ToList();
    }

    public T GetEntityById(Guid entityId)
    {
        return _dbSet.Find(entityId) ?? throw new InvalidOperationException();

    }

    public void AddNewEntity(T entity)
    {
        _dbSet.Add(entity);
    }

    public void UpdateEntity(T entity)
    {
        _dbSet.Attach(entity);
        _context.Entry(entity).State = EntityState.Modified;
    }

    public void DeleteEntity(Guid entityId)
    {
        var entity = GetEntityById(entityId);
        _dbSet.Remove(entity);
    }
}