namespace DataAccess.Repository.IRepository;

public interface IGenericRepository<T> where T : class
{
    public List<T> GetAll();
    public T GetEntityById(Guid entityId);
    public void AddNewEntity(T entity);
    public void UpdateEntity(T entity);
    public void DeleteEntity(Guid entityId);
}